import unittest
from selenium import webdriver
import time


class TestLesson(unittest.TestCase):
    def test_registration1(self, index=1):
        link = f"http://suninjuly.github.io/registration{index}.html"
        browser = webdriver.Chrome()
        browser.get(link)

        # Ваш код, который заполняет обязательные поля
        elements = []
        elements.append(browser.find_element_by_css_selector('input.first[required]'))
        elements.append(browser.find_element_by_css_selector('input.second[required]'))
        elements.append(browser.find_element_by_css_selector('input.third[required]'))
        for element in elements:
            element.send_keys("Мой")
        time.sleep(3)
        # Отправляем заполненную форму
        button = browser.find_element_by_css_selector("button.btn")
        button.click()

        # Проверяем, что смогли зарегистрироваться
        # ждем загрузки страницы
        time.sleep(1)

        # находим элемент, содержащий текст
        welcome_text_elt = browser.find_element_by_tag_name("h1")
        # записываем в переменную welcome_text текст из элемента welcome_text_elt
        welcome_text = welcome_text_elt.text

        # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта
        assert welcome_text == "Congratulations! You have successfully registered!", f"Ошибочка{index}"

    def test_registration2(self):
        self.test_registration1(index=2)


if __name__ == "__main__":
    unittest.main()
